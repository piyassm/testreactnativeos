import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, StyleSheet, Image} from 'react-native';

const ListView = () => {
  const [datas, setdatas] = useState([]);
  useEffect(async () => {
    let response = await fetch('https://jsonplaceholder.typicode.com/photos');
    let json = await response.json();
    setdatas(json);
  }, []);
  return (
    <View>
      <FlatList
        data={datas.map(data => ({
          key: data.id,
          title: data.title,
          image: data.thumbnailUrl,
        }))}
        renderItem={({item}) => (
          <View
            style={{
              flex: 1,
              marginTop: 20,
            }}>
            <Image
              source={{uri: item.image}}
              style={{
                width: 193,
                height: 110,
              }}
            />
            <Text>{item.title}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default ListView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
