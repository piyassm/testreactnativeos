import React from 'react';
import {StyleSheet, View, Button} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import Slider from './Slider';
import ListView from './ListView';

const Home = ({navigation, route}) => {
  function onLogout() {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'Login'}],
      }),
    );
  }
  return (
    <View style={styles.container}>
      <View style={styles.containerTop}>
        <Button
          title={'Logout'}
          style={{
            width: 200,
            height: 44,
          }}
          onPress={onLogout}
        />
      </View>
      <View style={styles.containerContent}>
        <Slider />
      </View>
      <View style={styles.containerContent}>
        <ListView />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ecf0f1',
  },
  containerTop: {
    alignItems: 'flex-end',
    padding: 0,
    margin: 0,
    backgroundColor: '#ecf0f1',
  },
  containerContent: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});

export default Home;
