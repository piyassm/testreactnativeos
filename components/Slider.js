import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Linking,
  Modal,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';

const Slider = ({navigation, route}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const onPressSlide = async index => {
    switch (index) {
      case 0:
        await Linking.openURL('https://www.thelivingos.com/');
        break;
      case 1:
        await Linking.openURL('https://www.google.com/maps');
        break;
      case 2:
        setModalVisible(true);
        break;
      default:
        break;
    }
  };
  const [datas, setDatas] = useState([
    {
      title: 'Item 1',
      text: 'Text 1',
      url:
        'https://images.pexels.com/photos/3877660/pexels-photo-3877660.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
      title: 'Item 2',
      text: 'Text 2',
      url:
        'https://images.pexels.com/photos/3756165/pexels-photo-3756165.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
      title: 'Item 3',
      text: 'Text 3',
      url:
        'https://images.pexels.com/photos/5067705/pexels-photo-5067705.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },
  ]);
  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          backgroundColor: 'floralwhite',
          borderRadius: 5,
          height: 250,
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
        }}>
        <TouchableOpacity onPress={() => onPressSlide(index)}>
          <Image
            style={{
              width: '100%',
              height: '100%',
            }}
            source={{
              uri: item.url,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <Image
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              width: '100%',
            }}
            source={{
              uri:
                'https://images.pexels.com/photos/5067705/pexels-photo-5067705.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            }}
            style={{width: '100%', height: '100%'}}
          />
        </View>
      </Modal>
      <Carousel
        data={datas}
        renderItem={renderItem}
        sliderWidth={300}
        itemWidth={300}
      />
    </View>
  );
};
export default Slider;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
