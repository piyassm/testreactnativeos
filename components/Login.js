import React, {useEffect, useState} from 'react';
import {StyleSheet, View, TextInput, Button, Image} from 'react-native';

const Login = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [disabled, setDisabled] = useState(true);

  function onLogin() {
    navigation.navigate('Home', {name: 'Home'});
    resetForm();
  }

  useEffect(() => {
    !!username.length && !!password.length && setDisabled(false);
    return () => {
      setDisabled(true);
    };
  }, [username, password]);

  useEffect(() => {
    return () => {
      resetForm();
    };
  }, []);
  const resetForm = () => {
    setUsername('');
    setPassword('');
  };

  return (
    <View style={styles.container}>
      <Image
        style={{
          width: 50,
          height: 50,
          marginBottom: 16,
        }}
        source={{
          uri: 'https://reactnative.dev/img/tiny_logo.png',
        }}
      />
      <TextInput
        value={username}
        onChangeText={value => setUsername(value)}
        placeholder={'Username'}
        style={styles.input}
      />
      <TextInput
        value={password}
        onChangeText={value => setPassword(value)}
        placeholder={'Password'}
        secureTextEntry={true}
        style={styles.input}
      />

      <Button
        title={'Login'}
        style={styles.input}
        onPress={onLogin}
        disabled={disabled}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
  },
});

export default Login;
